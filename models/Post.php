<?php

namespace app\models;

use Yii;
use yii\helpers\Html;

/**
 * This is the model class for table "post".
 *
 * @property int $id
 * @property string $subject
 * @property string $content
 * @property string $date
 * @property int $deleted
 * @property string $short
 */
class Post extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'post';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['subject'], 'required'],
            [['content'], 'string'],
            [['date'], 'safe'],
            [['deleted'], 'integer'],
            [['subject'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'subject' => 'Тема',
            'content' => 'Контент',
            'date' => 'Дата публікації',
            'deleted' => 'Видалена',
        ];
    }

    /**
     * If 'content' > 400 get shirt text (400) from 'content', else get 'content'
     *
     * @return string
     */
    public function getShort()
    {
        $count = strlen($this->content);
        return $count <= 400 ? $this->content : mb_strimwidth($this->content, 0, 400, '<b> ...</b>');

    }
}
