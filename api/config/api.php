<?php
$db = require(__DIR__ . '/../../config/db.php');
$params = [];

$config = [
    'id' => 'basic',
    'name' => 'ApiPost',
    'basePath' => dirname(__DIR__).'/..',
    'bootstrap' => ['log'],
    'components' => [
        'user' => [
            'identityClass' => 'app\api\modules\api\models\UserAPI',
            'enableSession' => false,
            'enableAutoLogin' => true,
        ],
        'request' => [
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ]
        ],
        'response' => [
            'class' => 'yii\web\Response',
            'format' => 'json',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                    'logFile' => '@app/runtime/logs/api.log',
                ],
            ],
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
//            'enableStrictParsing' => true,
            'enableStrictParsing' => false,
            'showScriptName' => false,
            'rules' => [
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => [
                        'api/user',
                        'api/post',
                    ],
                ],
                'GET,HEAD posts' => 'api/post/index',
                'GET,HEAD posts/<page:\d+>' => 'api/post/index',
                'GET,HEAD posts/<page:\d+>/<per-page:\d+>' => 'api/post/index',
                'GET,HEAD post/<id:\d+>' => 'api/post/view',
                'GET,HEAD posts/count-pages' => 'api/post/count-pages',
                'POST post' => 'api/post/create',
                'PUT,PATCH post/<id:\d+>' => 'api/post/update',
                'DELETE post/<id:\d+>' => 'api/post/delete',
                'GET,HEAD users' => 'api/user/index',
            ],
        ],
        'db' => $db,
    ],
    'modules' => [
        'api' => [
            'class' => 'app\api\modules\api\Api',
        ],
    ],
    'params' => $params,
];
return $config;