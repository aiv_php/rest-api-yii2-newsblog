<?php

namespace app\api\modules\api\controllers;

use Yii;
use app\api\modules\api\components\controllers\BaseApiController;
use yii\data\ActiveDataProvider;
use app\api\modules\api\models\PostAPI;
use yii\web\Response;

/**
 * API Class PostController
 *
 * @package app\api\modules\api\controllers
 */
class PostController extends BaseApiController
{
    public $modelClass = 'app\api\modules\api\models\PostAPI';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        return $behaviors;
    }

    /**
     * Set actions array
     *
     * @return array
     */
    public function actions()
    {
        $actions = parent::actions();
        unset($actions['index']);
        return $actions;
    }

    /**
     * Get count pages
     *
     * @param integer $per
     *
     * @return integer
     */
    public function actionCountPages($per = 5){
        $count = PostAPI::find()->where(['deleted'=>0])->count();
        $countPages = ceil($count/$per);
        Yii::$app->response->format = Response::FORMAT_JSON;
        return ['count'=>$countPages];
    }

    /**
     * Get all news models by PostAPI
     *
     * @return ActiveDataProvider
     */
    public function actionIndex()
    {
        $data = new ActiveDataProvider([
            'query' => PostAPI::find()->where(['deleted'=>0])->orderBy(['date'=>SORT_DESC]),
        ]);
        return $data;
    }

}