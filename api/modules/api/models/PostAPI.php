<?php

namespace app\api\modules\api\models;

use app\models\Post;
use Yii;

/**
 * Class PostAPI
 *
 * @package app\api\modules\api\models
 */
class PostAPI extends Post
{
    /**
     * API fields fo returne
     *
     * @return array
     */
    public function fields()
    {
        return [
            'id',
            'subject',
            'date',
            'content',
            'short'
        ];
    }
}
