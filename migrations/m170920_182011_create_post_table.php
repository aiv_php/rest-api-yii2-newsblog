<?php

use yii\db\Migration;

/**
 * Handles the creation of table `post`.
 */
class m170920_182011_create_post_table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%post}}', [
            'id' => $this->primaryKey(),
            'subject' => $this->string(255)->notNull(),
            'content' => $this->text(),
            'date' => $this->timestamp()->notNull(),
            'deleted' => $this->smallInteger(1)->defaultValue(0),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%post}}');
    }
}
