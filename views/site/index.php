<?php

/* @var $this yii\web\View */

$this->title = 'Новини AgriLab';
?>
<div class="site-index">

    <h1 style="margin: 0px">Новини AgriLab</h1>

    <hr>

    <input id="page" type="hidden" value="<?= $page ?>">
    <input id="per-page" type="hidden" value="<?= $perPage ?>">
    <input id="count-pages" type="hidden" value="">

    <div id="error-news">
    </div>

    <div class="list-news" id="list">
    </div>

    <div id="page-selection">
    </div>

</div>
