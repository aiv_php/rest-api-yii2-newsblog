<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\assets\AppAsset;
use app\widgets\Alert;
use kartik\datetime\DateTimePicker;
use vova07\imperavi\Widget;
use yii\bootstrap\Modal;
use yii\bootstrap\NavBar;
use yii\helpers\Html;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin(
        [
            'brandLabel' => 'AgriLab NEWS',
            'brandUrl'   => Yii::$app->homeUrl,
            'options'    => [
                'class' => 'navbar-inverse navbar-fixed-top',
            ],
        ]
    );
    echo Html::button(
        'Додати новину',
        [
            'class'       => 'btn btn-info navbar-btn pull-right',
            'data-toggle' => 'modal',
            'data-target' => '#createPost',
        ]
    );
    NavBar::end();
    ?>

    <div class="container">
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>
<!--Modal for create news-->
<?php
Modal::begin(
    [
        'id'     => 'createPost',
        'header' => '<h4 class="modal-title" id="myModalLabel">Створення новини</h4>',
        'footer' => Html::button(
                'Відмінити',
                ['class' => 'btn btn-default', 'data-dismiss' => 'modal']
            ).' '
            .Html::button(
                'Зберегти',
                ['class' => 'btn btn-default', 'id' => 'create-button']
            ),
    ]
); ?>

<div id="create-error">
</div>

<?= Html::beginForm('', 'POST', ['id' => 'createForm']) ?>

<div class="form-group field-subject required has-success">
    <label class="control-label" for="subject">Тема</label>
    <input type="text" id="subject" class="form-control" name="subject"
           aria-required="true" aria-invalid="false">
</div>

<div class="form-group field-date required has-success">
    <label class="control-label" for="date">Дата публікації</label>
    <?= DateTimePicker::widget(
        [
            'options'       => [
                'placeholder' => '...',
                'id'          => 'date',
            ],
            'name'          => 'date',
            'type'          => DateTimePicker::TYPE_INPUT,
            'pluginOptions' => [
                'autoclose' => true,
            ],
        ]
    ); ?>
</div>

<div class="form-group field-date required has-success">
    <label class="control-label" for="date">Контент</label>
    <?= Widget::widget(
        [
            'name'     => 'content',
            'settings' => [
                'lang'      => 'ru',
                'minHeight' => 150,
                'plugins'   => [
                    'clips',
                    'fullscreen',
                ],
            ],
        ]
    ); ?>
</div>

<?= Html::endForm(); ?>

<?php
Modal::end();
?>

<!--Modal ------------------>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; AIvDev <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
