/**
 * Created by AIvDeV on 21.09.2017.
 */

/**
 * Get list content
 */

url_list = 'http://agrilabapi.loc/posts';
url_count_pages = 'http://agrilabapi.loc/posts/count-pages';
url_create_news = 'http://agrilabapi.loc/post';


get_news_list = function () {
    var page = $('#page').val();
    var per_page = $('#per-page').val();

    $.ajax({
        url: url_list,
        type: 'GET',
        dataType: 'json',
        data: { "page": page, "per-page": per_page },
        success: function(data,status) {
            if(status == 'success'){

                $('#list').text('');
                $.each(data, function( index , value ) {
                    drow_item_news_list(value);
                });
            }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            if(XMLHttpRequest.responseJSON.message){
                var text_error = XMLHttpRequest.responseJSON.message;
            }else{
                var text_error = XMLHttpRequest.responseJSON[0].message;
            }
            var error = '<div class="alert alert-warning">' +
                '<b>Error!!!</b><br><b>' + errorThrown + ': </b>' + text_error +
                '</div>';
            $('#error-news').html('');
            $('#error-news').html(error);
            setTimeout( function () {$('#error-news').html('')} , 2000);
        }
    });
}


/**
 * Get item news list
 */
drow_item_news_list = function (itemList) {
    var content =   "<a class='btn btn-link pull-right' id='toggle-content-"+ itemList.id +"' onclick='show_full_content(" + itemList.id + ")'><span class='glyphicon glyphicon-plus'></span></a>" +
        "<div class='panel panel-body panel-info media' id='" + itemList.id + "'>" +
        "<div class='pull-left'>" +
        "<h4 class='panel-title'>" + itemList.date + "</h4>" +
        "</div>" +
                        "<div class='media-body'>" +
                            "<h4 class='media-heading'><b>" + itemList.subject + "</b></h4>" +
        "<div class='collapse' id='full-content-"+ itemList.id +"'>" +
        itemList.content +
        "</div>" +
        "<div class='collapse in' id='short-content-"+ itemList.id +"'>" +
        itemList.short +
        "</div>" +
                        "</div>" +
                    "</div>";
    $('#list').append(content);
}

/**
 * Show full version post.content
 */
show_full_content = function (idItem) {
    $('#short-content-'+idItem).collapse('hide');
    setTimeout(function(){$('#full-content-'+idItem).collapse('show')},500);
    $('#toggle-content-'+ idItem).html('<span class="glyphicon glyphicon-minus"></span>');
    $('#toggle-content-'+ idItem).removeAttr('onclick');
    $('#toggle-content-'+ idItem).attr('onclick','show_shirt_content('+idItem+')');
}

/**
 * Show shirt version post.content
 */
show_shirt_content = function (idItem) {
    $('#full-content-'+idItem).collapse('hide');
    setTimeout(function(){$('#short-content-'+idItem).collapse('show')},500);
    $('#toggle-content-'+ idItem).html('<span class="glyphicon glyphicon-plus"></span>');
    $('#toggle-content-'+ idItem).removeAttr('onclick');
    $('#toggle-content-'+ idItem).attr('onclick','show_full_content('+idItem+')');
}

/**
 * Get count news list pages
 */
get_count = function () {
    var per_page = $('#per-page').val();

    $.ajax({
        url: url_count_pages,
        type: 'GET',
        dataType: 'json',
        data: { "per": per_page },
        success: function(data,status,code) {
            if(status == 'success'){
                $('#count-pages').val(data.count);
                get_pagination();
            }else{
                $('#count-pages').val(1);
            }
        }
    });
}

/**
 * Show pagination
 */
get_pagination = function () {

    $('#page-selection').html('');
    $('#page-selection').bootpag({
        total: $('#count-pages').val(),
        maxVisible: $('#count-pages').val(),
        page: $('#page').val()
    }).on("page", function(event, num){
        $('#page').val(num);
        get_news_list();
    });
}

/**
 * Create news
 */
create_news = function () {
var form =  $('#createForm').serialize();
console.log(form);
    $.ajax({
        url: url_create_news,
        type: 'POST',
        dataType: 'json',
        data: form,
        success: function(data,status,code) {
            if(status == 'success'){
                $('#createPost').modal('hide');
                $('#createForm').trigger('reset');
                $('.redactor-editor').html('');
                $('#page').val(1);
                get_count();
                get_news_list();

            }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            if(XMLHttpRequest.responseJSON.message){
                var text_error = XMLHttpRequest.responseJSON.message;
            }else{
                var text_error = XMLHttpRequest.responseJSON[0].message;
            }
            var error = '<div class="alert alert-warning">' +
                '<b>Error!!!</b><br><b>' + errorThrown + ': </b>' + text_error +
            '</div>';
            $('#create-error').html('');
            $('#create-error').html(error);
            setTimeout( function () {$('#create-error').html('')} , 2000);
        }
    });

}

/**
 * Run default.
 */
$( document ).ready(function() {
    get_news_list();
    get_count();
    $('#create-button').click(function () {create_news()});
});

